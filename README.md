Gitserver
=========

installs and configures gitolite and openssh server within a container

Requirements
------------

* Docker
* Packer

Role Variables
--------------

Only the public key for ssh
* `ssh_user`
* `port`

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: grumps.gitserver }

License
-------

BSD

Author Information
------------------

grumps
